/****************************************************************************
** Meta object code from reading C++ file 'withleftmenuscreencontroller.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../WithLeftMenuWindow/withleftmenuscreencontroller.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'withleftmenuscreencontroller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_WithLeftMenuScreenController_t {
    QByteArrayData data[5];
    char stringdata0[89];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WithLeftMenuScreenController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WithLeftMenuScreenController_t qt_meta_stringdata_WithLeftMenuScreenController = {
    {
QT_MOC_LITERAL(0, 0, 28), // "WithLeftMenuScreenController"
QT_MOC_LITERAL(1, 29, 26), // "setCurrentLoggedInUserName"
QT_MOC_LITERAL(2, 56, 0), // ""
QT_MOC_LITERAL(3, 57, 4), // "name"
QT_MOC_LITERAL(4, 62, 26) // "getCurrentLoggedInUserName"

    },
    "WithLeftMenuScreenController\0"
    "setCurrentLoggedInUserName\0\0name\0"
    "getCurrentLoggedInUserName"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WithLeftMenuScreenController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,   24,    2, 0x02 /* Public */,
       4,    0,   27,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::QString,

       0        // eod
};

void WithLeftMenuScreenController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        WithLeftMenuScreenController *_t = static_cast<WithLeftMenuScreenController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setCurrentLoggedInUserName((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: { QString _r = _t->getCurrentLoggedInUserName();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

const QMetaObject WithLeftMenuScreenController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_WithLeftMenuScreenController.data,
      qt_meta_data_WithLeftMenuScreenController,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *WithLeftMenuScreenController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WithLeftMenuScreenController::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_WithLeftMenuScreenController.stringdata0))
        return static_cast<void*>(const_cast< WithLeftMenuScreenController*>(this));
    return QObject::qt_metacast(_clname);
}

int WithLeftMenuScreenController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
