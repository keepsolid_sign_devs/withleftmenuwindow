import QtQuick 2.0
import QtQuick.Controls 2.1

Popup {

    signal clickedOneDrive;
    signal clickedExplorer;
    signal clickedDropbox;
    signal clickedGoogleDrive;

    id: addDocumentPopup
    width: 230
    height: 230

    x: 0
    y: 0
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

    background: Rectangle {
        border.color: "#00000000"
        color: "#00000000"
    }

    Rectangle {
        id: backgroundRectangle
        x: 0
        y: 0
        width: 230
        height: 195
        color: "#ffffff"
        border.color: "#E6F0FA"
        border.width: 1

        Rectangle {
            id: rectangle
            color: "#ffffff"
            anchors.right: parent.right
            anchors.rightMargin: 30
            anchors.left: parent.left
            anchors.leftMargin: 30
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            anchors.top: parent.top
            anchors.topMargin: 15

            AddDocumentPopupButton {
                id: oneDriveRectangle
                color: "#ffffff"
                hoveredBackgroundColor: "#e6f0fa"
                backgroundColor: "#ffffff"
                isHovered: false
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 80
                anchors.right: parent.right
                anchors.rightMargin: 100
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0

                onClicked: {
                    addDocumentPopup.clickedOneDrive();
                }
            }

            AddDocumentPopupButton {
                id: explorerRectangle
                color: "#ffffff"
                hoveredBackgroundColor: "#e6f0fa"
                backgroundColor: "#ffffff"
                title: "Explorer"
                imageSource: "Resources/AddButtonIcons/iconAddDocExplorer@2x.png"
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 100
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 80
                anchors.top: parent.top
                anchors.topMargin: 0

                onClicked: {
                    addDocumentPopup.clickedExplorer();
                }
            }

            AddDocumentPopupButton {
                id: dropboxRectangle
                color: "#ffffff"
                hoveredBackgroundColor: "#e6f0fa"
                backgroundColor: "#ffffff"
                imageSource: "Resources/AddButtonIcons/iconAddDocDropbox@2x.png"
                title: "Dropbox"
                anchors.right: parent.right
                anchors.rightMargin: 100
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 80

                onClicked: {
                    addDocumentPopup.clickedDropbox();
                }
            }

            AddDocumentPopupButton {
                id: googleDriveRectangle
                color: "#ffffff"
                hoveredBackgroundColor: "#e6f0fa"
                backgroundColor: "#ffffff"
                title: "Google Drive"
                imageSource: "Resources/AddButtonIcons/iconAddDocGdrive@2x.png"
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 100
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 80

                onClicked: {
                    addDocumentPopup.clickedGoogleDrive();
                }
            }
        }

        AddButton {
            id: addButtonRectangle
            x: 96
            y: 194
            width: 134
            height: 36
            color: "#ffffff"
            hoveredBackgroundColor: "#ffffff"
            hoveredBorderColor: "#e6f0fa"
            borderColor: "#e6f0fa"
            backgroundColor: "#ffffff"
            borderWidth: 1
            border.color: "#e6f0fa"

            Rectangle {
                id: separatorRectangle
                height: 1
                color: "#ffffff"
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 1
                anchors.left: parent.left
                anchors.leftMargin: 1
                border.width: 0
            }

            onClicked: {
                addDocumentPopup.close();
            }
        }
    }
}
