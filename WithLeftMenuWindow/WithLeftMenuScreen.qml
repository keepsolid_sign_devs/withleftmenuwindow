import QtQuick.Window 2.2
import QtQuick 2.5
import QtQml 2.2
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.0
import "./" as Controls

Rectangle {
    id: mainScreenRect
    width: 800
    height: 550

    Connections {
        target: withLeftMenuScreenController

        onSelectedScreenIndexChanged: {

            if (selectedScreenIndex != leftMenulistView.currentIndex)
            {
                leftMenulistView.currentIndex = selectedScreenIndex;
                console.log(" --- qml --- WithLeftMenuScreenController::onSelectedScreenIndexChanged:", selectedScreenIndex);
            }
        }

        onCurrentLoggedInUserNameChanged: {
            //            loggedInUserNameRectangle.title = name;
            //            userMenuPopup.name = name;
        }

        onCurrentLoggedInUserEmailChanged: {
            //            userMenuPopup.email = email;
        }

        onCurrentLoggedInUserAvatarPathChanged: {
            //            userMenuPopup.icon = avatarPath;
            //            loggedInUserNameRectangle.imageSource = avatarPath;
        }

        onSyncDidEnd: {
            console.log(" --- qml --- onSyncDidEnd");
        }
    }

    onWidthChanged: {
        addDocumentPopup.x = mainScreenRect.width - addDocumentPopup.width - 10;
        addDocumentPopup.y = mainScreenRect.height - addDocumentPopup.height - 6;

        userMenuPopup.x = 260;
        userMenuPopup.y = 0;

        notificationsPopup.x = mainScreenRect.width - notificationsButton.anchors.rightMargin - notificationsPopup.width;
        notificationsPopup.y = notificationsButton.y;
    }

    onHeightChanged: {
        addDocumentPopup.x = mainScreenRect.width - addDocumentPopup.width - 10;
        addDocumentPopup.y = mainScreenRect.height - addDocumentPopup.height - 6;

        userMenuPopup.x = 260;
        userMenuPopup.y = 0;

        notificationsPopup.x = mainScreenRect.width - notificationsButton.anchors.rightMargin - notificationsPopup.width;
        notificationsPopup.y = notificationsButton.y;
    }

    NotificationsPopup {
        id: notificationsPopup
        padding: 0

        onClickedClearAll: {

            console.log(" --- qml --- onClickedClearAll:");
        }
    }

    UserMenuPopup {
        id: userMenuPopup
        padding: 0

        icon: withLeftMenuScreenController.currentLoggedInUserAvatarPath;
        name: withLeftMenuScreenController.currentLoggedInUserName;
        email: withLeftMenuScreenController.currentLoggedInUserEmail;

        onClickedSettings : {
            console.log(" --- qml --- onClickedSettings:")
            userMenuPopup.close();
            leftMenulistView.currentIndex = 6;
        }

        onClickedAbout : {
            console.log(" --- qml --- onClickedAbout:")
            userMenuPopup.close();
            withLeftMenuScreenController.aboutKeepSolidSignMenuItemDidClick();
        }

        onClickedLogout : {
            console.log(" --- qml --- onClickedLogout:")
            userMenuPopup.close();
            withLeftMenuScreenController.logoutMenuItemDidClick();
        }

        onClickedUserInfo : {
            console.log(" --- qml --- onClickedUserInfo:")
            userMenuPopup.close();
        }
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Please choose a file(s)")
        selectMultiple: true
        selectFolder: false
        folder: shortcuts.home
        nameFilters: [ "Pdf files (*.pdf)" ]
        selectedNameFilter: "All files (*)"

        onAccepted: {
            var filePathes = [];
            for (var i = 0; i < fileUrls.length; ++i)
            {
                console.log("\n --- qml --- file: " + (fileUrls[i]).toString().replace("file://", "") + "\n")
                filePathes.push((fileUrls[i]).toString().replace("file://", ""));
            }

            console.log(filePathes);
            withLeftMenuScreenController.addDocumentsFromExplorerDidClick(filePathes);
        }

        onRejected: {
            console.log("Canceled")
        }
    }

    AddDocumentPopup {
        id: addDocumentPopup
        padding: 0

        onClickedDropbox: {
            console.log(" --- qml --- onClickedDropbox:")
            addDocumentPopup.close();
            withLeftMenuScreenController.addDocumentsFromDropboxDidClick();
        }

        onClickedExplorer: {
            console.log(" --- qml --- onClickedExplorer:")
            addDocumentPopup.close();
            withLeftMenuScreenController.addDocumentsFromExplorerDidClick();
            fileDialog.visible = true;
        }

        onClickedOneDrive: {
            console.log(" --- qml --- onClickedOneDrive:")
            addDocumentPopup.close();
            withLeftMenuScreenController.addDocumentsFromOneDriveDidClick();
        }

        onClickedGoogleDrive: {
            console.log(" --- qml --- onClickedGoogleDrive:")
            addDocumentPopup.close();
            withLeftMenuScreenController.addDocumentsFromGoogleDriveDidClick();
        }
    }

    property Window withLeftMenuWindow
    property alias lockButtonRectangle: lockButtonRectangle
    property int currentPageIndex: 0
    property string loggedInUserName: withLeftMenuScreenController.currentLoggedInUserName;

    property ListModel leftMenuItemsList : ListModel {
        ListElement {
            name: "Documents"
            imageName: "iconMenuDocumentsNormal@2x.png"
            highlightedImageName: "iconMenuDocumentsActive@2x.png"
            sourceComponentForLoader: "DocumentsScreen.qml"
        }

        ListElement {
            name: "Templates"
            imageName: "iconMenuTemplatesNormal@2x.png"
            highlightedImageName: "iconMenuTemplatesActive@2x.png"
            sourceComponentForLoader: "TemplatesScreen.qml"
        }

        ListElement {
            name: "Archive"
            imageName: "iconMenuArchivesNormal@2x.png"
            highlightedImageName: "iconMenuArchivesActive@2x.png"
            sourceComponentForLoader: "ArchiveScreen.qml"
        }

        ListElement {
            name: "My signature"
            imageName: "iconMenuMySignatureNormal@2x.png"
            highlightedImageName: "iconMenuMySignatureActive@2x.png"
            sourceComponentForLoader: "SignaturesScreen.qml"
        }

        ListElement {
            name: "Contacts"
            imageName: "iconMenuContactsNormal@2x.png"
            highlightedImageName: "iconMenuContactsActive@2x.png"
            sourceComponentForLoader: "ContactsScreen.qml"
        }

        ListElement {
            name: "Feedback"
            imageName: "iconMenuFeedbackNormal@2x.png"
            highlightedImageName: "iconMenuFeedbackActive@2x.png"
            sourceComponentForLoader: "FeedbackScreen.qml"
        }

        ListElement {
            name: "Settings"
            imageName: "iconMenuSettingsNormal@2x.png"
            highlightedImageName: "iconMenuSettingsActive@2x.png"
            sourceComponentForLoader: "SettingsScreen.qml"
        }
    }

    Rectangle {
        id: leftMenuRectangle
        width: 230
        color: "#2980cc"
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0

        Rectangle {

            property bool isHovered: false

            id: lockButtonRectangle
            y: 502
            height: 48
            color: isHovered ? "#1863A0" : "#186bb4"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0

            Image {
                id: lockbuttonImage
                width: 12
                height: 16
                anchors.top: parent.top
                anchors.topMargin: 16
                anchors.left: parent.left
                anchors.leftMargin: 19
                source: "Resources/LockViewButtonIcons/iconLock@2x.png"
            }

            Text {
                id: lockButtonText
                x: 50
                y: 16
                width: 137
                height: 15
                text: qsTr("Lock")

                color: "white"

                font.weight: Font.Light
                font.pointSize: 12
                font.family: "OpenSans"
            }

            MouseArea {
                id: lockButtonRectangleMouseArea
                anchors.fill: parent

                hoverEnabled: true
                onEntered: lockButtonRectangle.isHovered = true
                onExited: lockButtonRectangle.isHovered = false

                onClicked: {
                    console.log(" --- qml --- lock button clicked ");
                    withLeftMenuScreenController.lockButtonDidClick();
                }
            }
        }

        Rectangle {
            id: logoImageRectangle
            height: 175
            color: "#2980cc"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0

            MouseArea {
                id: logoImageRectangleMouseArea
                anchors.fill: parent

                property variant previousPosition
                onPressed: {
                    previousPosition = Qt.point(mouseX, mouseY)
                }

                onPositionChanged: {
                    if (pressedButtons == Qt.LeftButton) {
                        var dx = mouseX - previousPosition.x
                        var dy = mouseY - previousPosition.y
                        mainScreenRect.withLeftMenuWindow.x = mainScreenRect.withLeftMenuWindow.x + dx
                        mainScreenRect.withLeftMenuWindow.y = mainScreenRect.withLeftMenuWindow.y + dy
                    }
                }

                onDoubleClicked:{
                    if (mainScreenRect.withLeftMenuWindow.visibility == Window.Maximized) {
                        mainScreenRect.withLeftMenuWindow.visibility = Window.AutomaticVisibility
                    } else {
                        mainScreenRect.withLeftMenuWindow.visibility = Window.Maximized
                    }
                }

                Image {
                    id: logoImage
                    width: 100
                    height: 100
                    anchors.left: parent.left
                    anchors.leftMargin: 65
                    anchors.top: parent.top
                    anchors.topMargin: 38
                    source: "Resources/LogoIcons/imgLogo@2x.png"
                }
            }
        }

        ListView {
            id: leftMenulistView
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 48
            anchors.top: parent.top
            anchors.topMargin: 175

            snapMode: ListView.SnapOneItem
            clip: true
            focus: true
            interactive: false

            model: mainScreenRect.leftMenuItemsList

            delegate:  Rectangle {

                property bool isHovered: false

                id: leftMenuListViewDelegateRactangle
                width: leftMenulistView.width
                height: 40
                color: (leftMenulistView.currentIndex == index) ? "white" : (isHovered ? "#186bb3" : "#2980cc")
                border.width: 0

                Image {
                    width: 20
                    height: 20
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                    source: "Resources/LeftMenuIcons/" + ((leftMenulistView.currentIndex == index) ? highlightedImageName : imageName)
                    smooth: true
                    anchors.verticalCenter: parent.verticalCenter
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: 50

                    text: name
                    color: (leftMenulistView.currentIndex == index) ? "#2980cc" : "white"

                    horizontalAlignment: Text.AlignLeft
                    font.weight: Font.Light
                    font.pointSize: 12
                    font.family: "OpenSans"
                }

                MouseArea {
                    id: leftMenuListViewDelegateRactangleMouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: leftMenuListViewDelegateRactangle.isHovered = true
                    onExited: leftMenuListViewDelegateRactangle.isHovered = false

                    onClicked: {
                        leftMenulistView.currentIndex = index
                    }
                }
            }

            highlightFollowsCurrentItem: true
            highlightMoveVelocity: 1000
            onCurrentIndexChanged: {

                mainScreenRect.currentPageIndex = currentIndex
                console.log(" --- qml --- mainScreenRect.currentPageIndex: ", mainScreenRect.currentPageIndex);

                withLeftMenuScreenController.selectedScreenIndex = currentIndex;

                screensLoader.source = "menuItemScreens/" + mainScreenRect.leftMenuItemsList.get(currentIndex).sourceComponentForLoader;

                serachTextFieldRectangle.text = withLeftMenuScreenController.getSearchStringForScreenAtIndex(currentIndex);

                switch (mainScreenRect.currentPageIndex) {

                case 0:
                    addButtonRectangle.visible = true
                    addButtonRectangle.title = "Add document"
                    break;

                case 1:
                    addButtonRectangle.visible = true
                    addButtonRectangle.title = "Add template"
                    break;

                case 2:
                    addButtonRectangle.visible = true
                    addButtonRectangle.title = "Add archive"
                    break;

                case 3:
                    addButtonRectangle.visible = false
                    break;

                case 4:
                    addButtonRectangle.visible = true
                    addButtonRectangle.title = "Add contact"
                    break;

                case 5:
                    addButtonRectangle.visible = false
                    break;

                case 6:
                    addButtonRectangle.visible = false
                    break;
                }
            }
        }
    }

    Rectangle {
        id: rightTopRectangle
        height: 30
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 230

        MouseArea {
            id: rightTopRectangleMouseArea
            anchors.fill: parent

            property variant previousPosition
            onPressed: {
                previousPosition = Qt.point(mouseX, mouseY)
            }

            onPositionChanged: {
                if (pressedButtons == Qt.LeftButton) {
                    var dx = mouseX - previousPosition.x
                    var dy = mouseY - previousPosition.y
                    mainScreenRect.withLeftMenuWindow.x = mainScreenRect.withLeftMenuWindow.x + dx
                    mainScreenRect.withLeftMenuWindow.y = mainScreenRect.withLeftMenuWindow.y + dy
                }
            }

            onDoubleClicked:{
                if (mainScreenRect.withLeftMenuWindow.visibility == Window.Maximized) {
                    mainScreenRect.withLeftMenuWindow.visibility = Window.AutomaticVisibility
                } else {
                    mainScreenRect.withLeftMenuWindow.visibility = Window.Maximized
                }
            }

            Rectangle {
                id: buttonsRect
                x: 370
                y: 0
                width: 154
                height: 30
                color: "#ffffff"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.right: parent.right
                anchors.rightMargin: 0

                Rectangle {
                    id: closeButtonRectangle
                    x: 110
                    width: 44
                    height: 27
                    color: "#ffffff"
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    anchors.right: parent.right
                    anchors.rightMargin: 0

                    MouseArea {
                        id: closeButtonMouseArea
                        x: 0
                        y: 0
                        width: 44
                        height: 27

                        onClicked: {
                            mainScreenRect.withLeftMenuWindow.close()
                        }

                        hoverEnabled: true
                        onEntered: {
                            parent.color = "#e7182b";
                            closeButtonImage.source = "Resources/RightTopRectIcons/iconWindowCloseLight@2x.png";
                        }
                        onExited: {
                            parent.color = "#ffffff";
                            closeButtonImage.source = "Resources/RightTopRectIcons/iconWindowClose@2x.png";
                        }

                        Image {
                            id: closeButtonImage
                            anchors.left: parent.left
                            anchors.leftMargin: 16
                            anchors.top: parent.top
                            anchors.topMargin: 7
                            source: "Resources/RightTopRectIcons/iconWindowClose@2x.png"
                        }
                    }
                }

                Rectangle {
                    id: maximizeButtonRectangle
                    x: 78
                    width: 32
                    height: 27
                    color: "#ffffff"
                    anchors.top: parent.top
                    anchors.topMargin: 0
                    anchors.right: parent.right
                    anchors.rightMargin: 44

                    MouseArea {
                        id: maximizeButtonMouseArea
                        anchors.fill: parent

                        onClicked: {

                            //                            if (mainScreenRect.withLeftMenuWindow.visibility == Window.FullScreen) {
                            //                                mainScreenRect.withLeftMenuWindow.visibility = Window.AutomaticVisibility
                            //                            } else {
                            //                                mainScreenRect.withLeftMenuWindow.visibility = Window.FullScreen
                            //                            }

                            if (mainScreenRect.withLeftMenuWindow.visibility == Window.Maximized) {
                                //                                mainScreenRect.withLeftMenuWindow.showNormal()
                                mainScreenRect.withLeftMenuWindow.visibility = Window.AutomaticVisibility
                            } else {
                                //                                mainScreenRect.withLeftMenuWindow.showMaximized()
                                mainScreenRect.withLeftMenuWindow.visibility = Window.Maximized
                            }

                        }

                        hoverEnabled: true
                        onEntered: {
                            parent.color = "#e6e6e6";
                            maximizeButtonImage.source = "Resources/RightTopRectIcons/iconWindowMaximizeDark@2x.png"
                        }
                        onExited: {
                            parent.color = "#ffffff";
                            maximizeButtonImage.source = "Resources/RightTopRectIcons/iconWindowMaximize@2x.png";
                        }

                        Image {
                            id: maximizeButtonImage
                            anchors.top: parent.top
                            anchors.topMargin: 7
                            anchors.left: parent.left
                            anchors.leftMargin: 10
                            source: "Resources/RightTopRectIcons/iconWindowMaximize@2x.png"
                        }
                    }
                }

                Rectangle {
                    id: minimizeButtonRectangle
                    x: 46
                    width: 32
                    height: 27
                    color: "#ffffff"
                    anchors.right: parent.right
                    anchors.rightMargin: 76
                    anchors.top: parent.top
                    anchors.topMargin: 0

                    MouseArea {
                        id: minimizeButtonMouseArea
                        anchors.fill: parent

                        onClicked: {
                            //                            mainScreenRect.withLeftMenuWindow.showMinimized();
                            mainScreenRect.withLeftMenuWindow.visibility = Window.Minimized
                        }

                        hoverEnabled: true
                        onEntered: {
                            parent.color = "#e6e6e6";
                            minimizeButtonImage.source = "Resources/RightTopRectIcons/iconWindowMinimizeDark@2x.png"
                        }
                        onExited: {
                            parent.color = "#ffffff";
                            minimizeButtonImage.source = "Resources/RightTopRectIcons/iconWindowMinimizeCopy@2x.png"
                        }

                        Image {
                            id: minimizeButtonImage
                            anchors.left: parent.left
                            anchors.leftMargin: 10
                            anchors.top: parent.top
                            anchors.topMargin: 7
                            source: "Resources/RightTopRectIcons/iconWindowMinimizeCopy@2x.png"
                        }
                    }
                }
            }

            LoggedInUserNameField {
                id: loggedInUserNameRectangle
                height: 30
                color: "#f8f8f8"
                anchors.top: parent.top
                anchors.topMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 30
                title: withLeftMenuScreenController.currentLoggedInUserName
                backgroundColor : "#f8f8f8"
                hoveredBackgroundColor: "#f6fbff"

                imageSource: withLeftMenuScreenController.currentLoggedInUserAvatarPath

                onClicked: {
                    console.log(" --- qml ---  logged in user name field clicked")

                    userMenuPopup.x = 260;
                    userMenuPopup.y = 0;
                    userMenuPopup.open();
                }
            }

            NotificationsButton {
                id: notificationsButton
                x: 437
                y: 6
                width: 20
                height: 20
                color: "#ffffff"
                imageSource : (notificationsController.notificationsCount() > 0) ? "Resources/NotificationsViewIcons/iconNotificationsNew@2x.png" : "Resources/NotificationsViewIcons/iconNotifications@2x.png"
                hoveredBorderWidth: 1
                hoveredBorderColor: "#e2f0fc"
                hoveredBackgroundColor: "#f6fbff"
                borderColor: "#ffffff"
                backgroundColor: "#ffffff"
                anchors.right: parent.right
                anchors.rightMargin: 113
                anchors.top: parent.top
                anchors.topMargin: 6

                Connections {
                    target: notificationsController

                    onNotificationsListDidChange: {

                        notificationsButton.imageSource = (notificationsController.notificationsCount() > 0) ? "Resources/NotificationsViewIcons/iconNotificationsNew@2x.png" : "Resources/NotificationsViewIcons/iconNotifications@2x.png";
                    }
                }

                onClicked: {
                    console.log(" --- qml --- notifications button clicked")

                    notificationsPopup.x = mainScreenRect.width - notificationsButton.anchors.rightMargin - notificationsPopup.width;
                    notificationsPopup.y = notificationsButton.y;
                    notificationsPopup.open();
                }
            }
        }

        SearchTextField {
            id: serachTextFieldRectangle
            x: 226
            width: 190
            height: 24
            color: "#ffffff"
            anchors.top: parent.top
            anchors.topMargin: 3
            rbImageForNotEmptyTextField: "Resources/RightTopRectIcons/btn_cancel.png"
            textColor: "#8d8d8d"
            borderColor: "#eaeaea"
            rbImageForEmptyTextField: "Resources/RightTopRectIcons/iconSearch@2x.png"
            placeholder: "Search documents..."
            borderWidth: 1
            anchors.right: parent.right
            anchors.rightMargin: 154

            onTextContentChanged: {
                console.log(" --- qml --- SearchTextField text:", text)
                withLeftMenuScreenController.searchStringDidChange(text);
            }
        }
    }

    Rectangle {
        id: rightBottomRectangle
        y: 502
        height: 48
        color: "#f8f8f8"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 230

        SyncButton {
            id: syncButtonRectangle
            width: 180
            color: "#f8f8f8"
            hoveredBackgroundColor: "#eaeaea"
            imageSource: "Resources/SyncButtonIcons/iconLastSync@2x.png"
            title: "<b>Last sync:</b> 7 June 2016"
            backgroundColor: "#f8f8f8"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 1
            anchors.left: parent.left
            anchors.leftMargin: 0

            onClicked: {
                console.log(" --- qml --- sync button clicked")
                withLeftMenuScreenController.syncButtonDidClick()
            }
        }

        Rectangle {
            id: topBorderRectangle
            height: 1
            color: "#eaeaea"
            anchors.right: parent.right
            anchors.rightMargin: 0
            anchors.left: parent.left
            anchors.leftMargin: 0
            anchors.top: parent.top
            anchors.topMargin: 0
        }

        AddButton {
            id: addButtonRectangle
            x: 426
            width: 134
            color: "#f8f8f8"
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 6
            anchors.top: parent.top
            anchors.topMargin: 6
            backgroundColor : "#f8f8f8"
            hoveredBackgroundColor: "#f6fbff"
            isHovered: false

            onClicked: {
                console.log(" --- qml ---  add button clicked")

                if (leftMenulistView.currentIndex == 0)
                {
                    addDocumentPopup.x = mainScreenRect.width - addDocumentPopup.width - 10;
                    addDocumentPopup.y = mainScreenRect.height - addDocumentPopup.height - 6;

                    addDocumentPopup.open();
                }
            }
        }

    }

    Rectangle {
        id: rightMiddleRectangle
        anchors.leftMargin: 230
        anchors.bottomMargin: 48
        anchors.topMargin: 30
        anchors.fill: parent

        Loader {
            id: screensLoader
            anchors.fill: parent
        }
    }

}
