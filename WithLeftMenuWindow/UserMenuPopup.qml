import QtQuick 2.0
import QtQuick.Controls 2.1

Popup {

    property int minWidth: 240
    property int maxWidth: 500

    property alias icon: avatarImage.source
    property alias name: nameText.text
    property alias email: emailText.text

    signal clickedSettings;
    signal clickedAbout;
    signal clickedLogout;
    signal clickedUserInfo;

    function longestText() {

        return (nameText.text.length >= emailText.text.length) ? nameText : emailText;
    }

    function recomendedWidth() {

        var maxTextWidth = longestText().width;
        var popupWidth = longestText().anchors.leftMargin + longestText().width + 20;

        if (popupWidth < minWidth)
            popupWidth = minWidth;

        if (popupWidth > maxWidth)
            popupWidth = maxWidth;

        return popupWidth;
    }

    id: userMenuPopup
    width: recomendedWidth();
    height: 175

    background: Rectangle {

        border.color: "#e2f0fc"
        border.width: 1
        color: "#00000000"
    }

    Rectangle {
        id: backgroundRectangle
        color: "#ffffff"
        anchors.right: parent.right
        anchors.rightMargin: 1
        anchors.left: parent.left
        anchors.leftMargin: 1
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        anchors.top: parent.top
        anchors.topMargin: 1
        border.width: 0

        Rectangle {
            id: userInfoRectangle
            x: 0
            y: 0
//            width: 238
//            width: userMenuPopup.width - 2
            width: parent.width
            height: 55
            color: "#ffffff"

            Image {
                id: avatarImage
                width: 36
                height: 36
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.top: parent.top
                anchors.topMargin: 10
                source: "Resources/UserMenuIcons/imgUserPhotoDefault@2x.png"
            }

            Text {
                id: nameText
//                width: 172
                color: "#444444"
                text: qsTr("Name Sername")
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 31
                anchors.left: parent.left
                anchors.leftMargin: 58
                anchors.top: parent.top
                anchors.topMargin: 12
                font.bold: true
                font.family: "Arial"
                font.pixelSize: 12
            }

            Text {
                id: emailText
//                width: 172
                color: "#444444"
                text: qsTr("v.teleusov@keepsolid.com")
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 13
                anchors.left: parent.left
                anchors.leftMargin: 58
                anchors.top: parent.top
                anchors.topMargin: 30
                font.pixelSize: 11
            }

            MouseArea {
                id: userInfoiMouseArea
                x: 0
                y: 0
                anchors.fill: parent

                onClicked: {
                    userMenuPopup.clickedUserInfo();
                }
            }
        }

        Rectangle {
            id: separatorRectangle
            x: 1
            y: 56
//            width: 238
//            width: userMenuPopup.width
            width: parent.width
            height: 1
            color: "#f5faff"
        }

        Rectangle {
            id: menuRectangle
            x: 0
            y: 69
//            width: 238
//            width: userMenuPopup.width
            width: parent.width
            height: 96
            color: "#ffffff"

            UserMenuItem {
                id: accountSettingsRectangle
                x: 0
                y: 0
//                width: 238
//                width: userMenuPopup.width
                width: parent.width
                height: 32
                color: "#ffffff"
                border.width: 0
                title: "Account Settings"
                textColor: "#2980cc"
                hoveredBackgroundColor: "#f9f9f9"
                backgroundColor: "#ffffff"

                onClicked: {
                    userMenuPopup.clickedSettings();
                }
            }

            UserMenuItem {
                id: aboutRectangle
                x: 0
                y: 32
//                width: 238
//                width: userMenuPopup.width
                width: parent.width
                height: 32
                color: "#ffffff"
                title: "About KeepSolid Sign"
                textColor: "#2980cc"
                hoveredBackgroundColor: "#f9f9f9"
                backgroundColor: "#ffffff"

                onClicked: {
                    userMenuPopup.clickedAbout();
                }
            }

            UserMenuItem {
                id: logoutRectangle
                x: 0
                y: 64
//                width: 238
//                width: userMenuPopup.width
                width: parent.width
                height: 32
                color: "#ffffff"
                title: "Logout"
                textColor: "#f02b2b"
                hoveredBackgroundColor: "#f9f9f9"
                backgroundColor: "#ffffff"

                onClicked: {
                    userMenuPopup.clickedLogout();
                }
            }
        }
    }
}
