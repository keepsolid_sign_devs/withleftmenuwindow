#ifndef NOTIFICATIONDATA_H
#define NOTIFICATIONDATA_H

#include <QObject>

enum CSNotificationType {
    CSNotificationTypeAd = 0,
    CSNotificationTypeTricks = 1,
    CSNotificationTypeMyDocumentSigned = 2,
    CSNotificationTypeMyDocumentFinished = 3,
    CSNotificationTypeDocumentPublishedForMe = 4,
    CSNotificationTypeMyTurnToSign = 5,
    CSNotificationTypeTrialPerionEnded = 6,
    CSNotificationTypeParticipantReadedDocument = 7,
    CSNotificationTypeParticipantFinishedSigning = 8,
    CSNotificationTypeDocumentRemoved = 9
};

class NotificationData
{
public:
    NotificationData();
    NotificationData(CSNotificationType _type = CSNotificationTypeAd,
                     unsigned int _timeStamp = 0,
                     unsigned int _id = 0,
                     QString _title = nullptr,
                     QString _subtitle = nullptr,
                     QString _iconSource = nullptr,
                     bool _isReaded = false);
private:

    CSNotificationType type;
    unsigned int timeStamp;
    unsigned int id;
    QString title;
    QString subtitle;
    QString iconSource;
    bool isReaded;
//    CSDocumentID* documentId;
//    CSUserID* author;
//    CSUserID* userId;

public:
    void setType(CSNotificationType _type);
    CSNotificationType getType();

    void setTimeStamp(unsigned int _timeStamp);
    unsigned int getTimeStamp();

    void setId(unsigned int _id);
    unsigned int getId() const;

    void setTitle(QString _title);
    QString getTitle() const;

    void setSubtitle(QString _subtitle);
    QString getSubtitle() const;

    void setIconSource(QString _iconSource);
    QString getIconSource() const;

    void setIsReaded(bool _isReaded);
    bool getIsReaded();
};

#endif // NOTIFICATIONDATA_H
