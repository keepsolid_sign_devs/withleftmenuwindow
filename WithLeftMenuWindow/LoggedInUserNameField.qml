import QtQuick 2.4
import QtQuick.Controls 2.2

Rectangle {

    property bool isHovered: false

    property color backgroundColor : "#f8f8f8"
    property color hoveredBackgroundColor : "#f6fbff"

    property color borderColor : "#00000000"
    property color hoveredBorderColor : "#e2f0fc"
    property int borderWidth : 0
    property int hoveredBorderWidth : 1

    property alias imageSource: loggedInUserImage.source
    property alias title: loggedInUserText.text

    signal clicked;

    id: loggedInUserNameRectangle
    width: loggedInUserText.anchors.leftMargin + loggedInUserText.width + 10
    height: 30

    MouseArea {
        id: loggedInUserNameMouseArea
        anchors.fill: parent

        hoverEnabled: true

        onEntered: {
            parent.color = parent.hoveredBackgroundColor;
            border.width = hoveredBorderWidth;
            border.color = hoveredBorderColor;
        }

        onExited: {
            parent.color = parent.backgroundColor;
            border.width = borderWidth;
            border.color = borderColor;
        }

        onClicked: {
            parent.clicked()
        }
    }

    Image {
        id: loggedInUserImage
        width: 18
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 6
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 6
        source: "Resources/RightTopRectIcons/iconUserPhoto@2x.png"
    }

    Text {
        id: loggedInUserText
        color: "#2980cc"
        text: "Alex Mnogosmislov"
        font.capitalization: Font.Capitalize
        font.weight: Font.Black
        font.family: "Arial"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 7
        anchors.top: parent.top
        anchors.topMargin: 8
        anchors.left: parent.left
        anchors.leftMargin: 36
        font.pixelSize: 12
    }
}
