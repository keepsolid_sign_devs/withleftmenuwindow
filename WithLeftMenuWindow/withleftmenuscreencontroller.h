#ifndef WITHLEFTMENUSCREENCONTROLLER_H
#define WITHLEFTMENUSCREENCONTROLLER_H

#include <QObject>
#include <QMap>

enum Screen {
    Screen_Documents = 0,
    Screen_Templates,
    Screen_Archive,
    Screen_Signatures,
    Screen_Contacts,
    Screen_Feedback,
    Screen_Settings
};

class WithLeftMenuScreenController : public QObject
{
    Q_OBJECT
public:
    WithLeftMenuScreenController(QObject *parent = 0);

private:
    QString currentLoggedInUserName;
    QString currentLoggedInUserEmail;
    QString currentLoggedInUserAvatarPath;
    unsigned int selectedScreenIndex;
    QMap <unsigned int, QString> searchStringForScreenAtIndex;

    Q_PROPERTY(QString currentLoggedInUserName
                WRITE setCurrentLoggedInUserName
                READ getCurrentLoggedInUserName
                NOTIFY currentLoggedInUserNameChanged
                )

    Q_PROPERTY(QString currentLoggedInUserEmail
                WRITE setCurrentLoggedInUserEmail
                READ getCurrentLoggedInUserEmail
                NOTIFY currentLoggedInUserEmailChanged
                )

    Q_PROPERTY(QString currentLoggedInUserAvatarPath
                WRITE setCurrentLoggedInUserAvatarPath
                READ getCurrentLoggedInUserAvatarPath
                NOTIFY currentLoggedInUserAvatarPathChanged
                )

    Q_PROPERTY(unsigned int selectedScreenIndex
                WRITE setSelectedScreenIndex
                READ getSelectedScreenIndex
                NOTIFY selectedScreenIndexChanged
                )
public:

    //public methods for using in C++:
    void selectScreenAtIndex(Screen index);

    // properties  definition for using in QML:
    void setCurrentLoggedInUserName(QString name);
    QString getCurrentLoggedInUserName();

    void setCurrentLoggedInUserEmail(QString email);
    QString getCurrentLoggedInUserEmail();

    void setCurrentLoggedInUserAvatarPath(QString avatarPath);
    QString getCurrentLoggedInUserAvatarPath();

    unsigned int getSelectedScreenIndex();
    void setSelectedScreenIndex(unsigned int index);

    // invokable methods for calling from QML:
    Q_INVOKABLE void lockButtonDidClick();
    Q_INVOKABLE void searchStringDidChange(QString string);
    Q_INVOKABLE QString getSearchStringForScreenAtIndex(unsigned int index);
    Q_INVOKABLE void syncButtonDidClick();
    Q_INVOKABLE void aboutKeepSolidSignMenuItemDidClick();
    Q_INVOKABLE void logoutMenuItemDidClick();

    Q_INVOKABLE void addDocumentsFromOneDriveDidClick();
    Q_INVOKABLE void addDocumentsFromExplorerDidClick();
    Q_INVOKABLE void addDocumentsFromDropboxDidClick();
    Q_INVOKABLE void addDocumentsFromGoogleDriveDidClick();

    Q_INVOKABLE void addDocumentsFromExplorerDidClick(const QList<QString> documentsToAddList);

signals:
    void currentLoggedInUserNameChanged(QString name);
    void currentLoggedInUserEmailChanged(QString email);
    void currentLoggedInUserAvatarPathChanged(QString avatarPath);
    void selectedScreenIndexChanged(unsigned int index);
    void syncDidEnd();
};

#endif // WITHLEFTMENUSCREENCONTROLLER_H
