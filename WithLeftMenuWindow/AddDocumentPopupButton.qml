import QtQuick 2.0

Rectangle {

    property bool isHovered: false

    property color backgroundColor : "#f8f8f8"
    property color hoveredBackgroundColor : "#dbdbdb"

    property alias imageSource: image.source
    property alias title: titleText.text

    signal clicked;

    id: addDocumentPopupButton
    width: 70
    height: 80
    color: "#ffffff"
    border.width: 0
    opacity: 0.8

    Image {
        id: image
        anchors.rightMargin: 17
        anchors.topMargin: 12
        anchors.bottomMargin: 32
        anchors.right: parent.right
        source: "Resources/AddButtonIcons/iconAddDocOnedrive@2x.png"
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 17
        anchors.bottom: parent.bottom
    }

    Text {
        id: titleText
        text: qsTr("OneDrive")
        font.family: "Arial"
        font.bold: true
        anchors.rightMargin: 0
        anchors.topMargin: 52
        font.pixelSize: 11
        anchors.bottomMargin: 15
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 0
        horizontalAlignment: Text.AlignHCenter
        anchors.bottom: parent.bottom
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        hoverEnabled: true

        onEntered: parent.color = parent.hoveredBackgroundColor;
        onExited: parent.color = parent.backgroundColor;

        onClicked: {
            parent.clicked()
        }
    }
}
