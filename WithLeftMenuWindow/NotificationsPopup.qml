import QtQuick 2.0
import QtQuick.Controls 2.1

Popup {

    signal clickedClearAll;

    property int popupMinRowHeight: 34
    property int popupListViewYPosition : 35
    property int popupMinHeight: (popupMinRowHeight + popupListViewYPosition)
    property int popupMaxHeight: 280

    function recomendedHeight() {

        var popupHeight = popupListViewYPosition + listView.contentHeight + listView.topMargin + listView.bottomMargin;
        popupHeight+=10;

        if (popupHeight <= popupMinHeight)
        {
            popupHeight = popupMinHeight;
        }
        else if (popupHeight >= popupMaxHeight)
        {
            popupHeight = popupMaxHeight;
        }

        return popupHeight;
    }

    id: notificationsPopup
    width: 200
    height: recomendedHeight();

    x: 0
    y: 0
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

    background: Rectangle {
        border.color: "#00000000"
        color: "#00000000"
    }

    Rectangle {
        id: backgroundRectangle
        color: "#ffffff"
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 19
        border.color: "#E6F0FA"
        border.width: 1

        Text {
            id: notificationsText
            width: 99
            height: 14
            text: qsTr("Notifications")
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 10
            font.bold: true
            font.family: "Arial"
            font.pixelSize: 12
        }

        MouseArea {
            id: clearAllMouseArea
            x: 120
            y: 11
            width: 70
            height: 12

            onClicked: {
                notificationsPopup.clickedClearAll();
                notificationsController.deleteAllNotifications();
            }
        }

        Text {
            id: clearAllText
            x: 120
            width: 70
            height: 12
            color: "#2980cc"
            text: qsTr("Clear all")
            anchors.top: parent.top
            anchors.topMargin: 11
            anchors.right: parent.right
            anchors.rightMargin: 10
            font.pointSize: 10
            font.family: "Arial"
            horizontalAlignment: Text.AlignRight
        }

        ListView {
            id: listView
            anchors.right: parent.right
            anchors.rightMargin: 1
            anchors.left: parent.left
            anchors.leftMargin: 1
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 1
            anchors.top: parent.top
            anchors.topMargin: 26

            snapMode: ListView.SnapOneItem
            clip: true
            focus: true

//            header: {
//                height: 0
//            }

//            footer: {
//                height: 0
//            }

            spacing: 0

//            section.delegate: Component {
//                    id: sectionHeading
//                    Rectangle {
//                        height: 0
//                    }
//                }

            model: notificationsController

            Connections {
                target: notificationsController

                onNotificationsListDidChange: {

                    console.log(" --- qml --- onNotificationsListDidChange notificationsButton.imageSource: ");
                    notificationsButton.imageSource = (notificationsController.notificationsCount() > 0) ? "Resources/NotificationsViewIcons/iconNotificationsNew@2x.png" : "Resources/NotificationsViewIcons/iconNotifications@2x.png";
                }
            }

            delegate: Rectangle {

                property int minHeight: 34
                property bool isHovered: false

                id: listViewDelegateRectangle
                width: listView.width
                height: (textId.height + 14 > minHeight) ? textId.height + 14 : minHeight
                border.width: 0
                color: isHovered ? "#f6fbff" : "white"

                Image {
                    id:image
                    width: 20
                    height: 20
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    anchors.top: parent.top
                    anchors.topMargin: 7
                    source: iconSourcePath
                    smooth: true
                }

                Text {
                    id:textId
                    anchors.left: parent.left
                    anchors.leftMargin: 38
                    anchors.top: parent.top
                    anchors.topMargin: 7
                    anchors.right: parent.right
                    anchors.rightMargin: 10

                    text: title
                    color: "#444444"
                    font.pointSize:10
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                }

                MouseArea {
                    id: listViewDelegateRectangleMouseArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: listViewDelegateRectangle.isHovered = true
                    onExited: listViewDelegateRectangle.isHovered = false

                    onClicked: {
                        listView.currentIndex = index

//                        notificationsController.deleteNotificationAtIndex(index);
                        var notificationId = notificationsController.notificationIdAtIndex(index);
                        notificationsController.deleteNotificationWithId(notificationId);
                    }
                }
            }
        }

        NotificationsButton {
            id: notificationsButton
            x: 180
            width: 20
            height: 20
            color: "#ffffff"
            borderColor: "#e2f0fc"
            hoveredBorderColor : "#e2f0fc"
            borderWidth: 1
            hoveredBorderWidth : 1
            hoveredBackgroundColor: "#ffffff"
            backgroundColor: "#ffffff"
            anchors.top: parent.top
            anchors.topMargin: -19
            anchors.right: parent.right
            anchors.rightMargin: 0
            imageSource: (notificationsController.notificationsCount() > 0) ? "Resources/NotificationsViewIcons/iconNotificationsNew@2x.png" : "Resources/NotificationsViewIcons/iconNotifications@2x.png"

            onClicked: {
                notificationsPopup.close();
            }

            Rectangle {
                id: rectangle
                y: 19
                height: 1
                color: "#ffffff"
                anchors.right: parent.right
                anchors.rightMargin: 1
                anchors.left: parent.left
                anchors.leftMargin: 1
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
            }
        }
    }
}
