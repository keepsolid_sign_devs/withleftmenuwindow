import QtQuick 2.0

Rectangle {

    property bool isHovered: false

    property color backgroundColor : "#f8f8f8"
    property color hoveredBackgroundColor : "#f6fbff"

    property color borderColor : "#00000000"
    property color hoveredBorderColor : "#e2f0fc"
    property int borderWidth : 0
    property int hoveredBorderWidth : 1

    property alias imageSource: image.source

    signal clicked;
    width: 20

    id: mainRectangle
    height: 20

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        hoverEnabled: true

        onEntered: {
            parent.color = parent.hoveredBackgroundColor;
            border.width = hoveredBorderWidth;
            border.color = hoveredBorderColor;
        }

        onExited: {
            parent.color = parent.backgroundColor;
            border.width = borderWidth;
            border.color = borderColor;
        }

        onClicked: {
            parent.clicked()
        }
    }

    Image {
        id: image
        width: 12
        height: 12
        anchors.left: parent.left
        anchors.leftMargin: 4
        anchors.top: parent.top
        anchors.topMargin: 4
//        source: "Resources/NotificationsViewIcons/iconNotificationsNew@2x.png"
    }
}
