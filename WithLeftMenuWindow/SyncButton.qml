import QtQuick 2.4
import QtQuick.Controls 2.2

Rectangle {

    property bool isHovered: false

    property color backgroundColor : "#f8f8f8"
    property color hoveredBackgroundColor : "#dbdbdb"

    property alias imageSource: syncButtonImage.source
    property alias title: syncButtonText.text

    signal clicked;

    id: syncButtonRectangle
    width: 180
    height: 47
    color: backgroundColor

    Image {
        id: syncButtonImage
        x: 30
        y: 17
        width: 14
        height: 14
        source: "Resources/SyncButtonIcons/iconLastSync@2x.png"

        RotationAnimator {
                    id: syncIconAnim
                    target: syncButtonImage;
                    from: 0;
                    to: 360;
                    loops: Animation.Infinite;
                    duration: 1000
                    running: false
                }
    }

    Text {
        id: syncButtonText
        x: 52
        y: 18
        width: 120
        height: 14
        color: "#444444"
        text: "<b>Last sync:</b> 7 June 2016"  //qsTr("Last sync: 7 June 2016")
        font.pixelSize: 10
        font.family: "OpenSans"
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        hoverEnabled: true

        onEntered: parent.color = parent.hoveredBackgroundColor;
        onExited: parent.color = parent.backgroundColor;

        onClicked: {
            parent.clicked()
            onClicked: syncIconAnim.running = !syncIconAnim.running
        }
    }
}
