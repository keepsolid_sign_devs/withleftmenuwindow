#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "withleftmenuscreencontroller.h"
#include "notificationscontroller.h"
#include "notificationdata.h"
#include <qquickview.h>
#include <QQuickView>
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

//    qmlRegisterType<WithLeftMenuScreenController>("com.cppcontrollers.withleftmenuscreencontroller", 1, 0, "WithLeftMenuScreenController");
//    qmlRegisterType<NotificationsController>("com.cppcontrollers.notificationscontroller", 1, 0, "NotificationsController");
//    qmlRegisterType<NotificationData>("com.cppcontrollers.notificationdata", 1, 0, "NotificationData");

    QQmlApplicationEngine engine;

    WithLeftMenuScreenController *wlsc = new WithLeftMenuScreenController();
    engine.rootContext()->setContextProperty("withLeftMenuScreenController", wlsc);

    NotificationsController *nc = new NotificationsController();
    engine.rootContext()->setContextProperty("notificationsController", nc);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
