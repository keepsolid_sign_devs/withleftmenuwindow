import QtQuick 2.4
import QtQuick.Controls 2.2

Rectangle {
    id: rectangle

    anchors.fill: parent
    color: "#ffffff"

    Text {
        id: text1
        x: 471
        width: 116
        height: 15
        text: "Contacts"
        anchors.top: parent.top
        anchors.topMargin: 37
        anchors.right: parent.right
        anchors.rightMargin: 53
        horizontalAlignment: Text.AlignRight
        font.pixelSize: 12
    }
}
