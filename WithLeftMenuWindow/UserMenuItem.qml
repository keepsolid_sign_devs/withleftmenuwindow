import QtQuick 2.0

Rectangle {

    property bool isHovered: false

    property color backgroundColor : "#f8f8f8"
    property color hoveredBackgroundColor : "#f6fbff"

    property alias textColor:  menuItemText.color
    property alias title: menuItemText.text

    signal clicked;

    id: menuItemRectangle
    width: 238
    height: 32
    color: "#ffffff"

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        hoverEnabled: true

        onEntered: {
            parent.color = parent.hoveredBackgroundColor;
        }

        onExited: {
            parent.color = parent.backgroundColor;
        }

        onClicked: {
            parent.clicked()
        }
    }

    Text {
        id: menuItemText
        color: "#2980cc"
        text: qsTr("Account Settings")
        anchors.right: parent.right
        anchors.rightMargin: 8
        anchors.left: parent.left
        anchors.leftMargin: 9
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.top: parent.top
        anchors.topMargin: 9
        font.pixelSize: 12
    }
}
