import QtQuick 2.4
import QtQuick.Controls 2.2

Rectangle {

    property bool isHovered: false

    property color backgroundColor : "#f8f8f8"
    property color hoveredBackgroundColor : "#f6fbff"

    property color borderColor : "#00000000"
    property color hoveredBorderColor : "#e2f0fc"
    property int borderWidth : 0
    property int hoveredBorderWidth : 1

    property alias imageSource: addButtonImage.source
    property alias title: addButtonText.text

    signal clicked;

    id: addButtonRectangle
    width: 134
    height: 36

    Image {
        id: addButtonImage
        anchors.right: parent.right
        anchors.rightMargin: 108
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        source: "Resources/AddButtonIcons/iconAddDocument@2x.png"
    }

    Text {
        id: addButtonText
        color: "#2980cc"
        text: qsTr("Add document")
        anchors.right: parent.right
        anchors.rightMargin: 11
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 34
        anchors.top: parent.top
        anchors.topMargin: 10
        font.pixelSize: 13
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent

        hoverEnabled: true

        onEntered: {
            parent.color = parent.hoveredBackgroundColor;
            border.width = hoveredBorderWidth;
            border.color = hoveredBorderColor;
        }

        onExited: {
            parent.color = parent.backgroundColor;
            border.width = borderWidth;
            border.color = borderColor;
        }

        onClicked: {
            parent.clicked()
        }
    }
}
