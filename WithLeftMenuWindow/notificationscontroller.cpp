#include "notificationscontroller.h"

NotificationsController::NotificationsController(QObject *parent) : QAbstractItemModel(parent), m_notificationsList()
{

    NotificationData nd_1 = NotificationData(CSNotificationTypeAd,
                                             0,
                                             0,
                                             "Signer has signed document",
                                             nullptr,
                                             "Resources/UserMenuIcons/imgUserPhotoDefault@2x.png",
                                             false);

    NotificationData nd_2 = NotificationData(CSNotificationTypeAd,
                                             0,
                                             1,
                                             "Your tern to sign documen \"document.pdf\"",
                                             nullptr,
                                             "Resources/UserMenuIcons/imgUserPhotoDefault@2x.png",
                                             false);

    NotificationData nd_3 = NotificationData(CSNotificationTypeAd,
                                             0,
                                             2,
                                             "You was succesfully signed the document",
                                             nullptr,
                                             "Resources/UserMenuIcons/imgUserPhotoDefault@2x.png",
                                             false);

    NotificationData nd_4 = NotificationData(CSNotificationTypeAd,
                                             0,
                                             3,
                                             "1. Signer has signed document. 2. Signer has signed document. 3. Signer has signed document.",
                                             nullptr,
                                             "Resources/UserMenuIcons/imgUserPhotoDefault@2x.png",
                                             false);

    addNotification(nd_1);
    addNotification(nd_2);
    addNotification(nd_3);
    addNotification(nd_4);
}

void NotificationsController::deleteAllNotifications()
{
    if (!m_notificationsList.isEmpty())
    {
        beginRemoveRows(QModelIndex(), 0, rowCount());

//        qDeleteAll(m_notificationsList.begin(), m_notificationsList.end());
        m_notificationsList.clear();

        endRemoveRows();

        emit notificationsListDidChange(m_notificationsList);
    }
}

void NotificationsController::deleteNotificationAtIndex(int index)
{
    if (!m_notificationsList.isEmpty() && rowCount() > index)
    {
        beginRemoveRows(QModelIndex(), index, index);
        m_notificationsList.removeAt(index);
        endRemoveRows();

        emit notificationsListDidChange(m_notificationsList);
    }
}

int NotificationsController::indexOfNotificationWithId(unsigned int id)
{
    int index = -1;

    for (int i = 0; i < m_notificationsList.size(); ++i) {
        if (m_notificationsList.at(i).getId() == id)
        {
            index = i;
            break;
        }
    }

    return index;
}

void NotificationsController::deleteNotificationWithId(unsigned int id)
{
    int notificationIndex = this->indexOfNotificationWithId(id);
    if (notificationIndex >= 0)
    {
        this->deleteNotificationAtIndex(notificationIndex);
    }
}

unsigned int NotificationsController::notificationIdAtIndex(int index)
{
    if (!m_notificationsList.isEmpty() && rowCount() > index)
    {
        NotificationData nd = m_notificationsList[index];
        return nd.getId();
    }

    return -1;
}

void NotificationsController::addNotification(NotificationData notificationDataToAdd)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_notificationsList.append(notificationDataToAdd);
    endInsertRows();

    emit notificationsListDidChange(m_notificationsList);
}

unsigned int NotificationsController::notificationsCount()
{
    return m_notificationsList.count();
}

// AbstractItemModel:

int NotificationsController::rowCount(const QModelIndex & parent) const {

    if (parent.isValid()) {

        return 0;
    }
    return m_notificationsList.size();
}

QVariant NotificationsController::data(const QModelIndex & index, int role) const {

    if (index.row() < 0 || index.row() >= m_notificationsList.size())
        return QVariant();

    NotificationData notificationData = m_notificationsList[index.row()];
    switch (role) {
    case NotificationTitleRole:
        return notificationData.getTitle();
        break;

    case NotificationImageSourceRole:
        return notificationData.getIconSource();
        break;

    case NotificationIdRole:
        return notificationData.getId();
        break;
    }

    return QVariant();
}

QHash<int, QByteArray> NotificationsController::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NotificationTitleRole] = "title";
    roles[NotificationImageSourceRole] = "iconSourcePath";
    roles[NotificationIdRole] = "id";
    return roles;
}

QModelIndex NotificationsController::index(int row, int column,
                  const QModelIndex &parent) const {

    Q_UNUSED(parent);
    return createIndex(row, column, static_cast<void *>(0));
}

QModelIndex NotificationsController::parent(const QModelIndex &child) const {

    Q_UNUSED(child);
    return QModelIndex();
}

int NotificationsController::columnCount(const QModelIndex &parent) const {

    Q_UNUSED(parent);
    return 1;
}

