import QtQuick 2.4
import QtQuick.Controls 2.2

Rectangle {

    property string rbImageForEmptyTextField : "Resources/RightTopRectIcons/iconSearch@2x.png"
    property string rbImageForNotEmptyTextField : "Resources/RightTopRectIcons/btn_cancel.png"
    property string placeholder : "Search documents..."

    property int borderWidth : 1
    property color borderColor : "#ebebeb"

    property alias text: textFieldID.text
    property alias textColor: textFieldID.color
    property alias textField: textFieldID

    signal textContentChanged;

    id: searchTextFieldID
    width: 190
    height: 24

    border.width: borderWidth
    border.color: borderColor
    radius: 0

    // 2. right button MouseArea
    MouseArea {
        id: mouseArea1
        x: 166
        y: 0
        width: 24
        height: 24
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        onClicked: {

            textFieldID.text = "";
        }
    }

    // 2. right button Image
    Image {

        id: rightImageID
        x: 172
        width: 12
        height: 12
        anchors.top: parent.top
        anchors.topMargin: 6
        anchors.right: parent.right
        anchors.rightMargin: 6
        source: (textFieldID.text.length == 0) ? parent.rbImageForEmptyTextField : parent.rbImageForNotEmptyTextField
        fillMode: Image.PreserveAspectFit
    }

    // 3. textfield
    TextField {

        id: textFieldID
        text: qsTr("")
        anchors.right: parent.right
        anchors.rightMargin: 24
        anchors.left: parent.left
        anchors.leftMargin: 2
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 2
        anchors.top: parent.top
        anchors.topMargin: 2
        padding: 1
        font.capitalization: Font.Capitalize
        font.bold: false
        font.weight: Font.Black
        font.family: "Tahoma"
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: 12
        placeholderText: parent.placeholder

        background: Rectangle {
            border.width: 0
        }

        onTextChanged: searchTextFieldID.textContentChanged();
    }
}
