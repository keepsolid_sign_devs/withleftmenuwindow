import QtQuick 2.5
import QtQml 2.2
import QtQuick.Controls 2.0
import QtQuick.Window 2.2
import "./" as Controls

Window {

    id: window
    visible: true
    width: 800
    height: 550
    minimumHeight: 550
    minimumWidth: 800
    title: qsTr("Hello World")
    flags: Qt.WindowMinMaxButtonsHint // | Qt.FramelessWindowHint | Qt.WindowSystemMenuHint

    Controls.WithLeftMenuScreen {
        anchors.fill: parent
        withLeftMenuWindow: window
    }
}
