#ifndef NOTIFICATIONSCONTROLLER_H
#define NOTIFICATIONSCONTROLLER_H

#include <QAbstractItemModel>
#include <QObject>
#include <QList>

#include "notificationdata.h"

class NotificationsController : public QAbstractItemModel
{
    Q_OBJECT
public:
    NotificationsController(QObject *parent = 0);
    enum DocumentRoles {

            NotificationTitleRole = Qt::UserRole + 1,
            NotificationImageSourceRole,
            NotificationIdRole
        };

    Q_INVOKABLE void deleteAllNotifications();
    Q_INVOKABLE void deleteNotificationAtIndex(int index);
    Q_INVOKABLE void deleteNotificationWithId(unsigned int id);
    Q_INVOKABLE int indexOfNotificationWithId(unsigned int id);
    Q_INVOKABLE unsigned int notificationIdAtIndex(int index);
    Q_INVOKABLE unsigned int notificationsCount();
    void addNotification(NotificationData notificationDataToAdd);


    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index,
                  int role = Qt::DisplayRole) const;
    QModelIndex index(int row,
                      int column,
                      const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &child) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

private:
    QList <NotificationData> m_notificationsList;

protected:

    QHash<int, QByteArray> roleNames() const;
signals:
    void notificationsListDidChange(QList <NotificationData> notificationsList);
};

#endif // NOTIFICATIONSCONTROLLER_H
