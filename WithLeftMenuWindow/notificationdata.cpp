#include "notificationdata.h"

NotificationData::NotificationData()
{

}

NotificationData::NotificationData(CSNotificationType _type,
                                   unsigned int _timeStamp,
                                   unsigned int _id,
                                   QString _title,
                                   QString _subtitle,
                                   QString _iconSource,
                                   bool _isReaded) :
    type(_type),
    timeStamp(_timeStamp),
    id(_id),
    title(_title),
    subtitle(_subtitle),
    iconSource(_iconSource),
    isReaded(_isReaded)
{

}

void NotificationData::setType(CSNotificationType _type)
{
    this->type = _type;
}

CSNotificationType NotificationData::getType()
{
    return this->type;
}

void NotificationData::setTimeStamp(unsigned int _timeStamp)
{
    this->timeStamp = _timeStamp;
}

unsigned int NotificationData::getTimeStamp()
{
    return this->timeStamp;
}

void NotificationData::setId(unsigned int _id)
{
    this->id = _id;
}

unsigned int NotificationData::getId() const
{
    return this->id;
}

void NotificationData::setTitle(QString _title)
{
    this->title = QString(_title);
}

QString NotificationData::getTitle() const
{
    return this->title;
}

void NotificationData::setSubtitle(QString _subtitle)
{
    this->subtitle = QString(_subtitle);
}

QString NotificationData::getSubtitle() const
{
    return this->subtitle;
}

void NotificationData::setIconSource(QString _iconSource)
{
    this->iconSource = QString(_iconSource);
}

QString NotificationData::getIconSource() const
{
    return this->iconSource;
}

void NotificationData::setIsReaded(bool _isReaded)
{
    this->isReaded = _isReaded;
}

bool NotificationData::getIsReaded()
{
    return this->isReaded;
}
