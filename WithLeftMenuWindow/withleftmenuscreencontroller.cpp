#include "withleftmenuscreencontroller.h"
#include <QDebug>
#include <QList>

WithLeftMenuScreenController::WithLeftMenuScreenController(QObject *parent) : QObject(parent), currentLoggedInUserName ("")
{
    this->setCurrentLoggedInUserName("Vitalii Tielieusov");
    this->setCurrentLoggedInUserEmail("v.teleusov@simplexsolutionsinc.com");
    this->setCurrentLoggedInUserAvatarPath("Resources/UserMenuIcons/imgUserPhotoDefault@2x.png");
    this->selectScreenAtIndex(Screen_Documents);

    searchStringForScreenAtIndex[Screen_Documents] = "";
    searchStringForScreenAtIndex[Screen_Templates] = "";
    searchStringForScreenAtIndex[Screen_Archive] = "";
    searchStringForScreenAtIndex[Screen_Signatures] = "";
    searchStringForScreenAtIndex[Screen_Contacts] = "";
    searchStringForScreenAtIndex[Screen_Feedback] = "";
    searchStringForScreenAtIndex[Screen_Settings] = "";
}

void WithLeftMenuScreenController::setCurrentLoggedInUserName(QString name)
{
    if (QString::compare(this->currentLoggedInUserName, name, Qt::CaseInsensitive) != 0)
    {
        this->currentLoggedInUserName = QString(name);
        emit currentLoggedInUserNameChanged(name);
    }
}

QString WithLeftMenuScreenController::getCurrentLoggedInUserName()
{
    return this->currentLoggedInUserName;
}

void WithLeftMenuScreenController::setCurrentLoggedInUserEmail(QString email)
{
    if (QString::compare(this->currentLoggedInUserEmail, email, Qt::CaseInsensitive) != 0)
    {
        this->currentLoggedInUserEmail = QString(email);
        emit currentLoggedInUserEmailChanged(email);
    }
}

QString WithLeftMenuScreenController::getCurrentLoggedInUserEmail()
{
    return this->currentLoggedInUserEmail;
}

void WithLeftMenuScreenController::setCurrentLoggedInUserAvatarPath(QString avatarPath)
{
    this->currentLoggedInUserAvatarPath = QString(avatarPath);
    emit currentLoggedInUserAvatarPathChanged(avatarPath);
}

QString WithLeftMenuScreenController::getCurrentLoggedInUserAvatarPath()
{
    return this->currentLoggedInUserAvatarPath;
}

unsigned int WithLeftMenuScreenController::getSelectedScreenIndex()
{
    return this->selectedScreenIndex;
}

void WithLeftMenuScreenController::setSelectedScreenIndex(unsigned int index)
{
    if (this->selectedScreenIndex != index)
    {
        this->selectedScreenIndex = index;
        emit selectedScreenIndexChanged(index);
    }

    qDebug(" --- qt --- did select screen at index: %d", index);
}

void WithLeftMenuScreenController::selectScreenAtIndex(Screen index)
{
    this->setSelectedScreenIndex(index);
}

void WithLeftMenuScreenController::lockButtonDidClick()
{
    qDebug(" --- qt --- lockButtonDidClick");
}

void WithLeftMenuScreenController::searchStringDidChange(QString string)
{
    searchStringForScreenAtIndex[selectedScreenIndex] = string;
    qDebug() << " --- qt --- searchStringDidChange:" << string << "at screen with index" << selectedScreenIndex;
}

QString WithLeftMenuScreenController::getSearchStringForScreenAtIndex(unsigned int index)
{
    return  searchStringForScreenAtIndex[index];
}

void WithLeftMenuScreenController::syncButtonDidClick()
{
    qDebug(" --- qt --- syncButtonDidClick");
}

void WithLeftMenuScreenController::aboutKeepSolidSignMenuItemDidClick()
{
    qDebug(" --- qt --- aboutKeepSolidSignMenuItemDidClick");
}

void WithLeftMenuScreenController::logoutMenuItemDidClick()
{
    qDebug(" --- qt --- logoutMenuItemDidClick");
}

void WithLeftMenuScreenController::addDocumentsFromOneDriveDidClick()
{
    qDebug(" --- qt --- addDocumentsFromOneDriveDidClick");
}

void WithLeftMenuScreenController::addDocumentsFromExplorerDidClick()
{
    qDebug(" --- qt --- addDocumentsFromExplorerDidClick");
}

void WithLeftMenuScreenController::addDocumentsFromExplorerDidClick(const QList<QString> documentsToAddList)
{
    qDebug(" --- qt --- addDocumentsFromExplorerDidClick: filesCount = %d", documentsToAddList.size());

    for (int i = 0; i < documentsToAddList.size(); ++i) {
        QString fileToAdd = documentsToAddList[i];
        qDebug("%d. %s", i+1, fileToAdd.toLatin1().constData());
    }
}

void WithLeftMenuScreenController::addDocumentsFromDropboxDidClick()
{
    qDebug(" --- qt --- addDocumentsFromDropboxDidClick");
}

void WithLeftMenuScreenController::addDocumentsFromGoogleDriveDidClick()
{
    qDebug(" --- qt --- addDocumentsFromGoogleDriveDidClick");
}
